#!/bin/sh
./bin/mg5_aMC prepare_run.txt
for counter in `seq 1 $N_RUNS`
do
    ./bin/mg5_aMC execute_run.txt
done
mv OUTPUTS outputs/.
chown $USRID:$GRPID -R outputs
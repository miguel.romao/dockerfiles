# HEP Dockerfiles

This is a repository for on-going efforts to containerise HEP applications with docker.

Currently this repository hosts de `Dockerfile` for

- MadGraph5 with Pythia8 and Delphes
- Jewel `2.2.0`, `2.4.0`

and other on-going efforts.


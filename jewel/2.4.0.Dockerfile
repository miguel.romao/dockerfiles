FROM rockylinux:9.3.20231119
RUN yum install -y wget make gcc gcc-c++ gcc-gfortran python python-devel
RUN wget https://LHAPDF.hepforge.org/downloads/?f=LHAPDF-6.5.4.tar.gz -O LHAPDF-6.5.4.tar.gz && tar -xvzf LHAPDF-6.5.4.tar.gz && rm LHAPDF-6.5.4.tar.gz
RUN wget https://jewel.hepforge.org/downloads/?f=jewel-2.4.0.tar.gz -O jewel-2.4.0.tar.gz && tar -xvzf jewel-2.4.0.tar.gz && rm jewel-2.4.0.tar.gz
WORKDIR "/LHAPDF-6.5.4"
RUN ./configure Pythia6LambdaV5Compat=true --prefix=/usr/local && make && make install
ENV LD_LIBRARY_PATH=/usr/local/lib/:/usr/local/lib64/:$LD_LIBRARY_PATH
ENV PYTHONPATH=/usr/local/lib64/python3.9/site-packages:$PYTHONPATH
RUN lhapdf install CT14nlo cteq6l1
WORKDIR "/jewel-2.4.0"
RUN make
ENV PATH="/jewel-2.4.0:${PATH}"
WORKDIR "/work"
CMD [ "jewel-2.4.0-vac" ]

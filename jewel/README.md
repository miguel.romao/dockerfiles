# Jewel Dockerfiles

This `Dockerfile` produces a docker image with Jewel versions `2.2.0` and `2.4.0`, running with `lhapdf` `5.9.1` and `6.5.4`, respectively.

The resulting images are available [here](https://hub.docker.com/repository/docker/mcromao/jewel/general).